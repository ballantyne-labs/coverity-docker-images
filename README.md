# cov_analysis_sharedrunner

This can be built using GitLab shared runners. With a free account you have 400 hours per month to use for building.

# cov_analysis_specificrunner

This needs to be built in GitLab specific runners rather than GitLab shared runners because the Coverity package together with the multistage build in Dockerfile requires a larger disk than the default 25GB in shared runners.

Once you have set up your specific runner, update the following:
/etc/gitlab-runner/config.toml
```
    privileged = true
    volumes = ["/cache", "/var/run/docker.sock:/var/run/docker.sock"]
```
